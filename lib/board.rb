class Board

  def self.default_grid
    @grid = Array.new(10) {|array| Array.new(10)}
  end

  def initialize (grid = Board.default_grid)
    @grid = grid
  end

  attr_reader :grid

  def [](position)
    @grid[position[0]][position[1]]
  end

  def count
    count = 0
    @grid.each do |array|
      array.each do |space|
        count += 1 if space == :s
      end
    end
    count
  end

  def empty?(position = nil)
    if position.nil? #no specific position
      @grid.each do |array|
        array.each do |space|
          return false if !space.nil?
        end
      end
      return true
    else #position given
    return true if @grid[position[0]][position[1]].nil?
    false
    end
  end

  def full?
    @grid.each do |array|
      array.each do |space|
        return false if space.nil?
      end
    end
    true
  end

  def place_random_ship
    raise 'error' if full?
    empty_spaces = []
    @grid.each_with_index do |array, gidx|
      array.each_with_index do |space, aidx|
        empty_spaces << [gidx, aidx] if space.nil?
      end
    end
    random_coordinates = empty_spaces.shuffle[0]
    @grid[random_coordinates[0]][random_coordinates[1]] = :s
  end

  def won?
    @grid.each do |array|
      array.each do |space|
        return false if space == :s
      end
    end
    true
  end
end #Board
