class BattleshipGame
  attr_reader :board, :player

  def initialize(player, board)
    @player = player
    @board = board
  end

  def attack(position)
    @board.grid[position[0]][position[1]] = :x
  end

  def count
    @board.count
  end

  def game_over?
    return true if @board.won?
    false
  end

  def play_turn
    attack(@player.get_play)
  end
end
